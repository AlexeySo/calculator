package com.example.calculator1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private int RESULT = 0;
    public static String EXTRA_MESSAGE = "EXTRA_MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Add(View view) {
        Intent intent = new Intent(this, resultActivity.class);
        EditText firstFactor = findViewById(R.id.firstFactor);
        EditText secondFactor = findViewById(R.id.secondFactor);
        int FirstFactor = Integer.parseInt(String.valueOf(firstFactor.getText()));
        int SecondFactor = Integer.parseInt(String.valueOf(secondFactor.getText()));
        RESULT = FirstFactor +  SecondFactor;
        String result = "";

        if (SecondFactor >= 0) {
            result = firstFactor.getText() + " + " + secondFactor.getText() + " = " + toString().valueOf(RESULT);
        } else {
            result = firstFactor.getText() + " + (" + secondFactor.getText() + ") = " + toString().valueOf(RESULT);
        }

        intent.putExtra(EXTRA_MESSAGE, result);
        startActivity(intent);
    }
}
